<?php

namespace Drupal\entity_type\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for entity type config entities.
 */
interface EntityTypeConfigInterface extends ConfigEntityInterface {

}
